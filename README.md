# spy-buildtools
This Docker image contains most of the tools, in tested configurations, that
are required to build Spy for most platforms.

## Bundled dependencies
* NodeJS (8.4.0)
* NPM (5.4.0)
* Mono (development only; 5.4.0)
* Wine (from Debian repositories)

Notice that no Node dependencies are listed in the list. You'll still have to
install them by yourself. Future releases may have some of the tools bundled.