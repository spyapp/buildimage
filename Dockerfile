FROM node

RUN dpkg --add-architecture i386 \
 && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
 && echo "deb http://download.mono-project.com/repo/debian jessie main" | tee /etc/apt/sources.list.d/mono-official.list \
 && apt-get update && apt-get install -y \
    wine \
    wine64 \
    wine32 \
    libwine:i386 \
    libwine \
    mono-devel \
 && rm -rf /var/lib/apt/lists/*

RUN npm install -g electron-installer-debian \
    css-loader \
    electron-packager \
    electron-winstaller \
    extract-text-webpack-plugin \
    file-loader \
    node-sass \
    sass-loader \
    style-loader \
    url-loader \
    webpack \
    webpack-closure-compiler \
    standard \
    standard-json

ENV PATH="${npm root -g}:${PATH}"